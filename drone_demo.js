var arDrone = require('ar-drone');
var client = arDrone.createClient();
var serialport = require("serialport");
var SerialPort = serialport.SerialPort;
var http    = require('http');
var fs         = require('fs');
var screenshot = require('node-webkit-screenshot');
var DroneImageRecognition = require( 'DroneImageRecognition' );
var isFlying = false;
var isStill = true;
var receivedData = '';


client.config('general:navdata_demo', 'FALSE');
//flat trim to initialize the drone
client.ftrim();

/*
  Here I declare the Bluetooth port on my computer
*/
portName = "/dev/cu.HC-06-DevB";

/*
  Here I create the serialport to be used
*/
var myPort = new SerialPort(portName, {
	baudRate: 9600,
	parser: serialport.parsers.readline("\n")
});


/*
  Here I send the sensor_data being read from the Bluetooth serial port
  to the function sendSerialData to process the data
*/
myPort.on('data', sendSerialData);

/*
  Function: sendSerialData(sensor_data)
  Here is where the ultrasonic sensor data is passed in as 
  sensor_data from the Bluetooth serial port. Based on which
  string is passed is which command to send the drone.
*/
function sendSerialData(data){ 
  if(receivedData != data.toString().trim()){
    receivedData = data.toString().trim();
    if(receivedData == "Stop"){
      console.log("Stop");
      
      //client.animateLeds('blinkRed', 5, 5); //use for milestone purposes

      /*
        Below is the flight commands that are used for the actual project
        If there is something in front of the drone it will stop (hover)
        and then back up and hover until another command is passed to it
      */
      if(!isStill){
         console.log("Stop and back up");
         isStill = true;
         client.stop();
         client
          .after(1000, function(){
            this.back(0.1);
          })
         client
          .after(1500, function(){
            this.stop();
          })
      }
      
    }

  }
  
}

/*
  The sensors are being "reset" every
  5 seconds to ensure that new objects are being detected
*/
function reset_senors(){
    receivedData = '';
};

setInterval(reset_senors, 5000); 


/*
  Utilize the pngStream functionality of continuously
  streaming images from the drone's front camera  
*/
console.log('Connecting png stream ...');

var pngStream = arDrone.createClient().getPngStream();

var lastPng;
pngStream
  .on('error', console.log)
  .on('data', function(pngBuffer) {
    lastPng = pngBuffer;

  });

/*
  writes the image to a file and then calls DroneImageRecognition.checkForCommand to 
  process it and return a command string 
*/
 write_file = function(){
 	fs.writeFile('command2_real.png', lastPng, function(){
  		console.log("screenshot is saved...");
  	});
  	DroneImageRecognition.checkForCommand( './build/RecognizeCommand' , 'command2_real.png' , my_callback );
 }

/*
  Wait 2 seconds and then write the image to a file so that it is not 
  continuously writing
*/
setInterval(write_file, 2000); 
 
/*
  This displays the image on a web server to view it on localhost:8080
  which helps with testing and troubleshooting by providing more
  visibility
*/
var server = http.createServer(function(req, res) {
  if (!lastPng) {
    res.writeHead(503);
    res.end('Did not receive any png data yet.');
    return;
  }

  res.writeHead(200, {'Content-Type': 'image/png'});
  res.end(lastPng);
});

server.listen(8080, function() {
  console.log('Serving latest png on port 8080 ...');
});

/*
  Function: my_callback
  This function will take in the command returned from DroneImageRecognition.checkForCommand
  and will send a command to the drone based on the string, command.

*/
var my_callback = function( error , command ){
    //if there was an error; print it out
    if( error ){
        console.log( 'Error: ' + error );
        return;
    }

    /*
      Here a command is passed to the drone based on
      which string was returned from the DroneImageRecognition node module
    */
    if(command === "command1"){
    	console.log( 'take off... ' + command);


      //Used for my milestone task to prove a command is being passed 
      //to the drone. It flashes alternating green and red lights 
     // client.animateLeds('blinkGreenRed', 5, 5);
      
      /*
        Below is the actual flight commands that will be used
        in the demo. Upon seeing the image for the first time
        the drone will take off and then move up in altitude and 
        hover. The second time it sees the image it will land.
        It will alternate functionality every time it sees command1.
      */
      if(isFlying == false){
        isStill = true;
    		isFlying = true;
    		client.takeoff();
   		
    	}
    	else{
    		isFlying = false;
    		console.log( 'landing... ' + command);
    		client.land();
    	}
    	
		
   			

    }
    if(command === "command2"){

      isStill = false;
    	console.log( 'move towards target ' + command);


      //Used to demonstrate my milestone task, this command
      //will make the drone flash orange lights
      //client.animateLeds('blinkOrange', 5, 5);
      
      /*
        The actual flight commands that will be used in the
        demo. Upon seeing command2, the drone will move towards it.
      */

      client.front(0.1);
      

    		    	
    }
    if(command === "no_command"){
    	console.log( 'nothing ' + command);

    }
   
   
}







  

  


