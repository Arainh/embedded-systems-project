from DroneImageRecognition import checkForCommand

#pass in the path to the image file to process
output = checkForCommand( "../res/command2_real.jpg" )
print output

if output == 'command1':
    #do something

elif output == 'command2':
    #do something else

else:
    #if we didn't find command1 or command2, then no_command was returned. So just continue on doing other stuff
