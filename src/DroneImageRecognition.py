import shlex
from subprocess import Popen, PIPE



def checkForCommand( image_path ):
    cmd = "../build/RecognizeCommand " + image_path
    process = Popen( shlex.split( cmd ) , stdout=PIPE , stderr=PIPE )
    ( output , err ) = process.communicate()
    exit_code = process.wait()

    if( err.find( 'command1' ) != -1 ):
        return "command1"

    elif( err.find( 'command2' ) != -1 ):
        return "command2"

    else:
        return "no_command"



