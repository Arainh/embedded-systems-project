//This is how we include my node module
var DroneImageRecognition = require( 'DroneImageRecognition' );



//Since Node.js runs ansynchronously, we need to create a callback. This is the callback that will
//run when the 'checkForCommand' function finishes. When my function finishes, it will call this callback
//and pass in: 1. any error it got and 2. the command it found
var my_callback = function( error , command ){

    //if there was an error; print it out
    if( error ){
        console.log( 'Error: ' + error );
        return;
    }

    //otherwise, we can do something. Here we just print the command.
    console.log( 'Command: ' + command );
};



//This is the function that runs the command recognition. It takes 3 arguments:
//      1. the programs name
//      2. the path to the image you want to check for a command
//      3. a callback (like the one we defined up top)
DroneImageRecognition.checkForCommand( './build/RecognizeCommand' , 'command2_real.png' , my_callback );