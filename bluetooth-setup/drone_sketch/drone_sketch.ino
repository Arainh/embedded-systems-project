/*This program will communicate to an arduino */
#include <SoftwareSerial.h>
// front sensor
#define trigPin 8 // Trigger Pin
#define echoPin 7 // Echo Pin
// left sensor
#define trigPin2 6
#define echoPin2 5
// right sensor
#define trigPin3 4
#define echoPin3 3
#define LEDPin 13
int distanceLimit = 75;
long duration, distance, duration2, distance2, duration3, distance3;
long counter = 0, counter2 = 0, counter3 = 0, counter4 = 0, counter5 = 0, counter6 = 0, counter7 = 0;
void setup() {
 Serial.begin (9600);
 pinMode(trigPin, OUTPUT);
 pinMode(echoPin, INPUT);
 //pinMode(trigPin2, OUTPUT);
 //pinMode(echoPin2, INPUT);
 //pinMode(trigPin3, OUTPUT);
 //pinMode(echoPin3, INPUT);
 //pinMode(LEDPin, OUTPUT);
 
}
void loop() {
/* The following trigPin/echoPin cycle is used to determine the
 distance of the nearest object by bouncing soundwaves off of it. */ 
 digitalWrite(trigPin, LOW); 
 delayMicroseconds(2); 
 digitalWrite(trigPin, HIGH);
 delayMicroseconds(10); 
 
 digitalWrite(trigPin, LOW);
 /*Read a pulse when the echoPin value is HIGH. It returns the
   length of the pulse in microseconds from when it started as
   HIGH and then changed to LOW.*/
 duration = pulseIn(echoPin, HIGH);
 
 /*Calculate the distance (in cm) based on the speed of sound.
   The duration is the amount of time in microseconds for the 
   pulse to go from HIGH to LOW. This is converted to centimeters.
   The speed of sound is 344 m/s which converts to .0344 cm/s,
   which means 29 microseconds per centimeter. The duration must
   be divided in half to calculate the distance one way. This would
   be duration/29/2 = duration/58 */
 distance = duration/58;
 /* Send the distance to the computer using Serial protocol */
 //Serial.print("front: ");
 //Serial.println(distance);
 /*
 digitalWrite(trigPin2, LOW); 
 delayMicroseconds(2); 
 digitalWrite(trigPin2, HIGH);
 delayMicroseconds(10); 
 
 digitalWrite(trigPin2, LOW);
 duration2 = pulseIn(echoPin2, HIGH);
 distance2 = duration2/58;
 //Serial.print("left: ");
 //Serial.println(distance2);
 
 digitalWrite(trigPin3, LOW);
 delayMicroseconds(2);
 digitalWrite(trigPin3, HIGH);
 delayMicroseconds(10); 
 digitalWrite(trigPin3, LOW);
 //duration3 = pulseIn(echoPin3, HIGH);
 //distance3 = duration3/58;
 //Serial.print("right: ");
 //Serial.println(distance3);
 */
 /*If the distance from the sensor is less than 25 cm, turn
   on LED light, indicating it is too close, otherwise turn
   the LED light off if distance exceeds 25 cm*/

 /*If the distance from the sensors is less than 25 cm, take
  * action after waiting some constant amount of time to avoid
  * running into incoming obstacles
  */
  if ((distance <= distanceLimit) )
  {
    // There is an obstacle on the front, left, and right. Turn around.
    
      counter++;
      delay(50);
      if (counter % 2 == 0){
        Serial.println("Stop");
        //Serial.println("180");
      
    }
  }
    /*
    // There is an obstacle on the front and left. Go right.
    else if (distance <= distanceLimit && distance2 <= distanceLimit){
      counter2++;
      delay(50);
      if (counter2 % 2 == 0){
        //Serial.println("Stop");
        Serial.println("Right");
      }
    }
    // There is an obstacle on the front and right. Go left.
    else if (distance <= distanceLimit && distance3 <= distanceLimit){
      counter3++;
      delay(50);
      if (counter3 % 2 == 0){
        //Serial.println("Stop");
        Serial.println("Left");
      }
    }
    // There is an obstacle on the left and right. Go forward.
    else if (distance2 <= distanceLimit && distance3 <= distanceLimit){
      counter4++;
      delay(50);
      if (counter4 % 2 == 0){
        //Serial.println("Stop");
        Serial.println("Forward");
      }
    }
    // There is an obstacle on the front. Go back.
    // I'm considering adding rotate 135 degrees clockwise then move right.
    else if (distance <= distanceLimit){
      counter5++;
      delay(50);
      if (counter5 % 2 == 0){
        //Serial.println("Stop");
        Serial.println("Back");
      }  
    }
    // There is an obstacle on the left. Go right.
    else if (distance2 <= distanceLimit){
      counter6++;
      delay(50);
      if (counter6 % 2 == 0){
        //Serial.println("Stop");
        Serial.println("Right");
      }
    }
    // There is an obstacle on the right. Go left.
    else if (distance3 <= distanceLimit){
      counter7++;
      delay(50);
      if (counter7 % 2 == 0){
        //Serial.println("Stop");
        Serial.println("Left");
      }
    }
    // No obstacle.
    /*
    else {
      delay(50);
      {
        Serial.println("No"); 
      } 
    }
    */
  
  
  
 //Delay 50ms before next reading.
}

